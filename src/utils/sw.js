const { assets } = global.serviceWorkerOption;
const isDev = process.env.NODE_ENV === 'development';

const CACHE_NAME = webpack.env.CACHE_KEY;
const URLS_TO_CACHE = [global.location.href].concat(assets.map(path => {
    return new URL(`/assets${path}`, global.location).toString()
}));
const CACHE_REGEX = /\/(assets|images)\//;

self.addEventListener('install', event => {
    const cache$ = caches.open(CACHE_NAME).then(cache => cache.addAll(URLS_TO_CACHE)).then(() => self.skipWaiting());
    event.waitUntil(cache$);
});

self.addEventListener('activate', event => {
    const cache$ = caches.keys().then(cacheNames => Promise.all(
        cacheNames.map(cacheName => cacheName.indexOf(CACHE_NAME) === 0 ? null : caches.delete(cacheName))
    )).then(() => self.clients.claim());

    event.waitUntil(cache$);
});

self.addEventListener('fetch', event => {
    const request = event.request;

    if (!CACHE_REGEX.test(request.url)) {
        return;
    }

    if (request.method !== 'GET') {
        return;
    }

    const requestUrl = new URL(request.url);
    if (requestUrl.origin !== location.origin) {
        return;
    }

    const response$ = caches.match(event.request).then(response => {
        if (isDev) {
            return fetch(request);
        }

        return response || fetch(request);
    });

    event.respondWith(response$);
});
