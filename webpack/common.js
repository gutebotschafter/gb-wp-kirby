const { join } = require('path');
const webpack = require('webpack');
const ServiceWorkerPlugin = require('serviceworker-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const recursiveIssuer = m => {
    if (m.issuer) {
        return recursiveIssuer(m.issuer);
    } else if (m.name) {
        return m.name;
    } else {
        return false;
    }
};

module.exports = {
    output: {
        filename: '[name].bundle.js',
        path: join(__dirname, '../public/assets'),
        chunkFilename: '[name].chunk.js',
        publicPath: '/assets/'
    },
    resolve: {
        extensions: ['.js', '.json'],
        modules: [join(__dirname, '../node_modules'), join(__dirname, '../src')]
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                appStyles: {
                    name: 'app',
                    test: (m, c, entry = 'general') =>
                        m.constructor.name === 'CssModule' && recursiveIssuer(m) === entry,
                    chunks: 'all',
                    enforce: true,
                },
                criticalStyles: {
                    name: 'criticals',
                    test: (m, c, entry = 'criticals') =>
                        m.constructor.name === 'CssModule' && recursiveIssuer(m) === entry,
                    chunks: 'all',
                    enforce: true,
                },
            },
        },
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.(png|jpg|jpeg|svg|gif|ico|webp)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: 'images/[name].[ext]'
                    }
                }
            },
            {
                test: /\.(woff|woff2|ttf|eot|otf)/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]'
                    }
                }
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: true,
                            reloadAll: true
                        },
                    },
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            config: {
                                path: join(
                                    __dirname,
                                    '..',
                                    'config.js'
                                )
                            }
                        }
                    },
                    'sass-loader',
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].bundle.css',
        }),
        new ServiceWorkerPlugin({
            entry: join(__dirname, '..', 'src', 'utils', 'sw.js'),
            publicPath: '/',
            filename: '../sw.js'
        }),
        new webpack.DefinePlugin({
            'webpack.env.CACHE_KEY': JSON.stringify(`sw-cache-${Date.now()}`)
        })
    ]
};
