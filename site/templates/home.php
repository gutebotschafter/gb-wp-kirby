<?php snippet('header') ?>

<main class="page-main">

  <?php foreach ($site->children()->filterBy('template', 'not in', ['default'])->listed() as $item): ?>
    <?php snippet('ce/' . $item->template(), ['data' => $item]) ?>
  <?php endforeach ?>

</main>

<?php snippet('footer') ?>
