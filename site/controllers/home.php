<?php

return function ($kirby, $pages, $page, $site) {

  $alert = null;

    if($kirby->request()->is('POST') && get('submit')) {

    // check the honeypot
    if(empty(get('website')) === false) {
      go($page->url());
      exit;
    }

    $data = [
      'name'  => get('name'),
      'email' => get('email'),
      'url'   => get('url'),
      'phone' => get('phone')
    ];

    $rules = [
      'name'  => ['required', 'min' => 3],
      'email' => ['required', 'email']
    ];

    $messages = [
      'name'  => 'Bitte geben Sie einen Namen ein',
      'email' => 'Bitte geben Sie eine korrekte E-Mail-Adresse ein',
      'url'   => 'Bitte tragen Sie Ihre Webseite ein',
      'phone' => 'Bitte tragen Sie Ihre Telefonnummer ein'
    ];

    // some of the data is invalid
    if($invalid = invalid($data, $rules, $messages)) {
      $alert = $invalid;

      // the data is fine, let's send the email
      } else {
        try {
          $kirby->email([
            'template' => 'email',
            'from'     => 'noreply@gute-botschafter.de',
            'replyTo'  => $data['email'],
            'to'       => $site->email()->value,
            'subject'  => 'Neue Kontaktanfrage von ' . esc($data['name']) . ' auf der ' . $site->title()->value . ' Webseite',
            'data'     => [
              'name'    => esc($data['name']),
              'email'   => esc($data['email']),
              'url'     => esc($data['url']),
              'phone'   => esc($data['phone']),
              'title'   => $site->title()->value
            ]
          ]);

        } catch (Exception $error) {
            $alert['error'] = "Das Versenden einer E-Mail über das Formular ist leider fehlgeschlagen. Bitte versuchen Sie es zu einem späteren Zeitpunkt noch einmal.";
            var_dump ($error->getMessage());
        }

      // no exception occured, let's send a success message
      if (empty($alert) === true) {
          $success = 'Ihre Anfrage wurde erfolgreich versendet. Wir melden uns umgehend bei Ihnen.';
          $data = [];
      }
    }
  }

  return [
    'alert'   => $alert,
    'data'    => $data ?? false,
    'success' => $success ?? false
  ];

};
