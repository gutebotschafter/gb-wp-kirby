    <?php if($page->isHomepage()): ?>
      <?php snippet('page/page-contact') ?>
    <?php endif ?>

    <?php snippet('page/page-footer') ?>

    <?= js('/assets/app.bundle.js') ?>

  </body>

</html>
