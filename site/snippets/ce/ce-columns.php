<div class="ce-columns" id="<?= $data->slug() ?>">
  <div class="ce-columns__title">
    <h2><?= $data->headline()->kti() ?></h2>
    <?php if($data->sub()->isNotEmpty()): ?>
      <sub><?= $data->sub()->html() ?></sub>
    <?php endif ?>
  </div>
  <div class="ce-columns__text">
    <?= $data->text()->kirbytext() ?>
  </div>
</div>
