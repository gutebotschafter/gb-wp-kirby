<figure class="ce-image ce-image--<?= $data->orientation()->html() ?>" id="<?= $data->slug() ?>">
  <img class="ce-image__image" src="<?= $data->picture()->toFile()->thumb(['width' => 1440, 'height' => 720, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $data->picture()->toFile()->alt()->html() ?>">
</figure>
