<div class="ce-text" id="<?= $data->slug() ?>">
  <?php if($data->headline()->isNotEmpty()): ?>
    <div class="ce-text__title">
      <h2><?= $data->headline()->kti() ?></h2>
      <?php if($data->sub()->isNotEmpty()): ?>
        <sub><?= $data->sub()->html() ?></sub>
      <?php endif ?>
    </div>
  <?php endif ?>
  <div class="ce-text__text">
    <?= $data->text()->kirbytext() ?>
  </div>
</div>
